;
(function () {
    "use strict";

    // customizable part
    var targetClass = 'scrollToTop',
        scrollTime = 800,
        topOffset = 0,
        appearingOffset = 300,
        buttonType = 'text', // text or image
        content = '&#8593;', // img url or text to show
        easing = 'swing'; // http://gsgd.co.uk/sandbox/jquery/easing/ for more info

    $('.' + targetClass).css({
        'right' : '40px',
        'bottom' : '75px',
        'font-size' : '200%',
        'padding' : '10px',
        'text-align' : 'center',
        'background' : 'rgba(255,255,255,0)',
        'color' : '#333',
        'text-decoration' : 'none',
        'position' : 'fixed',
        'display' : 'none'
    });
    // end of customizable part


    /*the div is empty at the beginning
      to prevent appearing it 
      before script is loaded */

    var htmlContent = "";
    if (buttonType === "image") {
        htmlContent = '<img src="' + content + '">';
    } else if (buttonType === "text") {
        htmlContent = content;
    }

    $('.' + targetClass).html(htmlContent);

    $(window).scroll(function () {
        if ($(this).scrollTop() > appearingOffset) {
            $('.' + targetClass).fadeIn();
        } else {
            $('.' + targetClass).fadeOut();
        }
    });

    $('.' + targetClass).click(function () {
        $('html, body').animate({
            scrollTop: topOffset
        }, scrollTime, easing);
        return false;
    });
}());